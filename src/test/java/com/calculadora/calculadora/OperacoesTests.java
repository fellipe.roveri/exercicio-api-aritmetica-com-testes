package com.calculadora.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {

    @Test
    public void testarOperacaoDeSomaLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.soma(numeros), 6);
    }

    @Test
    public void testarSomaDeDoisNumeros(){
        Assertions.assertEquals(Operacao.soma(1,1), 2);
    }

    @Test
    public void testarOperacaoDeSubtracaoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(3);
        numeros.add(1);
        numeros.add(1);
        Assertions.assertEquals(Operacao.subtracao(numeros), 1);
    }

    @Test
    public void testarOperacaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.subtracao(1,1), 0);
    }

    @Test
    public void testarOperacaoDeMultiplicacaoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(3);
        numeros.add(1);
        numeros.add(1);
        Assertions.assertEquals(Operacao.multiplicacao(numeros), 3);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.multiplicacao(1, 1), 1);
    }

    @Test
    public void testarOperacaoDeDivisaoLista() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(4);
        numeros.add(2);
        numeros.add(2);
        Assertions.assertEquals(Operacao.divisao(numeros), 1);
    }

    @Test
    public void testarDivisãoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.divisao(4, 2), 2);
    }


}
