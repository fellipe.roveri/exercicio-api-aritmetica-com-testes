package com.calculadora.calculadora;

import java.util.List;

public class Operacao {

    public static int soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            resultado += numero;
        }
        return resultado;
    }

    public static int soma(int num1, int num2){
        int resultado = num1 + num2;
        return resultado;
    }

    public static int subtracao(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            if (resultado == 0){
                resultado = numero;
            }else {
                resultado -= numero;
            }
        }
        return resultado;
    }

    public static int subtracao(int num1, int num2){
        int resultado = num1 - num2;
        return resultado;
    }

    public static int multiplicacao(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            if (resultado == 0){
                resultado = numero;
            }else {
                resultado *= numero;
            }
        }
        return resultado;
    }

    public static int multiplicacao(int num1, int num2){
        int resultado = num1 * num2;
        return resultado;
    }

    public static int divisao(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            if (resultado == 0){
                resultado = numero;
            }else {
                resultado /= numero;
            }
        }
        return resultado;
    }

    public static int divisao(int num1, int num2){
        int resultado = num1 / num2;
        return resultado;
    }
}
